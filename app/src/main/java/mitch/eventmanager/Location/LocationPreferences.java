package mitch.eventmanager.Location;

import javax.inject.Inject;

import mitch.eventmanager.Misc.SharedEditor;

/**
 * Created by paulmitchell on 30/04/2017.
 */

public class LocationPreferences {

    @Inject
    SharedEditor sharedEditor;

    private String key = "EVENT_LOCATION_";

    private String eventID;

    public LocationPreferences(String eventID) {
        this.eventID = eventID;
        key = key + eventID;
    }

    public Boolean getLocationSetting() {
        return false;
    }

    public boolean checkUserLocationSetting() {
        return false;
    }

}
