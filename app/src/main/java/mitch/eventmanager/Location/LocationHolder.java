package mitch.eventmanager.Location;

/**
 * Created by paulmitchell on 30/04/2017.
 */

public class LocationHolder {

    private Double lng;
    private Double lat;
    private Double accuracy;

    public LocationHolder(Double lng, Double lat, Double accuracy) {
        this.lng = lng;
        this.lat = lat;
        this.accuracy = accuracy;
    }

    public Double getLng() {
        return lng;
    }

    public Double getLat() {
        return lat;
    }

    public Double getAccuracy() {
        return accuracy;
    }

}
