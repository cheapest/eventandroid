package mitch.eventmanager.ChatManager.Holder;


public class Message {

    private String message;
    private String user;
    private String sent;
    private String eventID;

    public Message(String message, String user, String sent, String eventID) {
        this.message = message;
        this.sent = sent;
        this.user = user;
        this.eventID = eventID;
    }

    public String getEventID() {
        return eventID;
    }

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getSent() {
        return sent;
    }

    public void setSent(String sent) {
        this.sent = sent;
    }

    public String getMessage() {

        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
