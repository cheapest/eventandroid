package mitch.eventmanager.ChatManager.Exception;

/**
 * Created by paulmitchell on 17/04/2017.
 */

public class ChatException extends Exception {

    public ChatException(String message) {
        super(message);
    }


}
