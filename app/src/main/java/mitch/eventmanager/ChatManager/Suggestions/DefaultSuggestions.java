package mitch.eventmanager.ChatManager.Suggestions;

/**
 * Created by paulmitchell on 17/04/2017.
 */

public class DefaultSuggestions {

    public String[] greetings = new String[]{
            "Hi!",
            "Hello Everyone!",
            "How Is Everyone Today?"
    };

    public String[] defaultSuggestions = new String[]{
            "Be There In 5 Minutes",
            "I'll be a bit late...",
            "On My Way",
            "Who's Going?",
            "What Time Are People Getting there?",
            "Wait For Me, I'm Almost There!",
            "Almost There.",
            "Where Is Everybody?"
    };

    /*
        TO DO:
        - Work on a machine learning system to calculate suggestions based on previous chats
     */

}
