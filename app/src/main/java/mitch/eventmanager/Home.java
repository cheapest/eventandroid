package mitch.eventmanager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mitch.eventmanager.API.APIClient;
import mitch.eventmanager.API.Interfaces.EventInterface;
import mitch.eventmanager.API.Response.EventResponse.HomeEventResponse;
import mitch.eventmanager.Event.Holder.HomeEvent;
import mitch.eventmanager.Event.List.HomePageAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Home extends BaseActivity {

    private HomePageAdapter myEventsRecyclerAdapter;

    private HomePageAdapter recommendedRecyclerAdapter;

    private HomePageAdapter nearByRecyclerAdapter;

    private AdapterSetup adapterSetup;

    private EventInterface eventInterface;


    @SuppressLint("MissingSuperCall")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_home);
        if (!isLoggedin) {
            //Send back to login
            Intent i = new Intent(this, Login.class);
            startActivity(i);
            finish();
        }

        eventInterface = APIClient.getClient(URL).create(EventInterface.class);

        initialisePage();
        setupEvents();
    }

    private void initialisePage() {

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager layoutManager3 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        //Init myEvents
        RecyclerView myEventsRecyclerView = (RecyclerView) findViewById(R.id.myEventsList);
        myEventsRecyclerAdapter = new HomePageAdapter(this);
        myEventsRecyclerView.setAdapter(myEventsRecyclerAdapter);
        myEventsRecyclerView.setLayoutManager(layoutManager1);
        SnapHelper myEventsSnap = new LinearSnapHelper();
        myEventsSnap.attachToRecyclerView(myEventsRecyclerView);

        //Init recommended
        RecyclerView recommendedRecyclerView = (RecyclerView) findViewById(R.id.recommendedEventList);
        recommendedRecyclerAdapter = new HomePageAdapter(this);
        recommendedRecyclerView.setAdapter(recommendedRecyclerAdapter);
        recommendedRecyclerView.setLayoutManager(layoutManager2);
        SnapHelper recommendedSnap = new LinearSnapHelper();
        recommendedSnap.attachToRecyclerView(recommendedRecyclerView);

        //init nearby public events
        RecyclerView nearByRecyclerView = (RecyclerView) findViewById(R.id.eventsNearMeList);
        nearByRecyclerAdapter = new HomePageAdapter(this);
        nearByRecyclerView.setAdapter(nearByRecyclerAdapter);
        nearByRecyclerView.setLayoutManager(layoutManager3);
        SnapHelper nearbySnap = new LinearSnapHelper();
        nearbySnap.attachToRecyclerView(nearByRecyclerView);


    }



    /* public void setupEmptyEvents() {
        myEventsRecyclerAdapter.insert(new HomeEvent(true));
        recommendedRecyclerAdapter.insert(new HomeEvent(true));
        nearByRecyclerAdapter.insert(new HomeEvent(true));
    }
    */

    public void setupEvents() {
        adapterSetup = new AdapterSetup();
        getMyEvents();
        //not implemented yet
        // getRecommendedEvents();
        recommendedRecyclerAdapter.insert(new HomeEvent(true));
        getNearbyEvents();
    }

    public void getMyEvents() {
        final ProgressBar pro = (ProgressBar) findViewById(R.id.progressBar1);
        pro.setVisibility(View.VISIBLE);

        Call<HomeEventResponse> getMyEvents = eventInterface.getMyEvents(getAuthHeader());

        getMyEvents.enqueue(new Callback<HomeEventResponse>() {
            @Override
            public void onResponse(Call<HomeEventResponse> call, Response<HomeEventResponse> response) {

                HomeEventResponse homeEventResponse = response.body();

                if (response.isSuccessful() ||
                        (homeEventResponse != null && !homeEventResponse.isSuccess() ||
                                homeEventResponse.getCount() == 0)) {

                    Log.d("RESPONSE TAG", "My Events Error");
                    myEventsRecyclerAdapter.insert(new HomeEvent(true));

                } else {
                    myEventsRecyclerAdapter = adapterSetup.addEvents(myEventsRecyclerAdapter, response.body().getEvents());
                }

                pro.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<HomeEventResponse> call, Throwable t) {
                snackbox.showMessage("Error Result", t.getMessage());
                myEventsRecyclerAdapter.insert(new HomeEvent(true));
            }
        });


    }

    public void getRecommendedEvents() {

        Call<HomeEventResponse> getRecommendations = eventInterface.getRecommendation(getAuthHeader());

        getRecommendations.enqueue(new Callback<HomeEventResponse>() {
            @Override
            public void onResponse(Call<HomeEventResponse> call, Response<HomeEventResponse> response) {

                HomeEventResponse homeEventResponse = response.body();

                if (response.isSuccessful() ||
                        (homeEventResponse != null && !homeEventResponse.isSuccess() ||
                                homeEventResponse.getCount() == 0)) {

                    Log.d("RESPONSE TAG", "Recommendation Error");
                    recommendedRecyclerAdapter.insert(new HomeEvent(true));

                } else {
                    recommendedRecyclerAdapter = adapterSetup.addEvents(recommendedRecyclerAdapter, response.body().getEvents());
                }

            }

            @Override
            public void onFailure(Call<HomeEventResponse> call, Throwable t) {
                snackbox.showMessage("Error Result", t.getMessage());
                recommendedRecyclerAdapter.insert(new HomeEvent(true));
            }
        });

    }

    public void getNearbyEvents() {
        String url = URL + "/event/nearby";

        Map<String, String> params = new HashMap<>();
        params.put("userLocationLat", Double.toString(mLatitude));
        params.put("userLocationLon", Double.toString(mLongitude));
        //Events within 10 Miles
        params.put("radius", "10");


        Call<HomeEventResponse> getNearby = eventInterface.getNearbyEvents(getAuthHeader(),
                Double.toString(mLatitude),
                Double.toString(mLongitude),
                10);

        getNearby.enqueue(new Callback<HomeEventResponse>() {
            @Override
            public void onResponse(Call<HomeEventResponse> call, Response<HomeEventResponse> response) {

                HomeEventResponse homeEventResponse = response.body();

                if (response.isSuccessful() ||
                        (homeEventResponse != null && !homeEventResponse.isSuccess() ||
                                homeEventResponse.getCount() == 0)) {

                    Log.d("RESPONSE TAG", "Nearby Events Error");
                    nearByRecyclerAdapter.insert(new HomeEvent(true));

                } else {
                    nearByRecyclerAdapter = adapterSetup.addEvents(nearByRecyclerAdapter, response.body().getEvents());
                }

            }

            @Override
            public void onFailure(Call<HomeEventResponse> call, Throwable t) {
                snackbox.showMessage("Error Result", t.getMessage());
                recommendedRecyclerAdapter.insert(new HomeEvent(true));
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }


    private class AdapterSetup {

        HomePageAdapter addEvents(HomePageAdapter toSetup, List<HomeEvent> response) {

            if (response.size() == 0) {
                // Should never reach here.
                // Backup just incase
                toSetup.insert(new HomeEvent(true));

            } else {

                for (HomeEvent homeEvent : response) {

                    toSetup.insert(homeEvent);
                }

            }

            return toSetup;
        }

    }

}

