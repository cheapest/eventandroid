package mitch.eventmanager.API.Interfaces;

import mitch.eventmanager.API.Request.EventModifyRequest;
import mitch.eventmanager.API.Response.EventResponse.EventResponse;
import mitch.eventmanager.API.Response.EventResponse.HomeEventResponse;
import mitch.eventmanager.API.Response.SimpleResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface EventInterface {

    @POST("event")
    Call<SimpleResponse> addEvent(@Header("Authorization") String authorization, @Body EventModifyRequest eventModifyRequest);


    @PUT("events/{eventID}")
    Call<SimpleResponse> updateEvent(@Header("Authorization") String authorization, @Body EventModifyRequest eventModifyRequest);

    @GET("event/{eventID}")
    Call<EventResponse> getEvent(@Header("Authorization") String authorization, @Path("eventID") String eventID);

    @Headers("Content-Type:application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("event/nearby")
    Call<HomeEventResponse> getNearbyEvents(@Header("Authorization") String authorization,
                                            @Field("userLocationLat") String userLocationLat,
                                            @Field("userLocationLon") String userLocationLon,
                                            @Field("radius") int radius);

    @GET("event/recommend")
    Call<HomeEventResponse> getRecommendation(@Header("Authorization") String authorization);

    @GET("event/myEvents")
    Call<HomeEventResponse> getMyEvents(@Header("Authorization") String authorization);

    @POST("")
    Call<SimpleResponse> changeVenueResponse(@Header("Authorization") String authorization,
                                             @Field("venueID") String venueID,
                                             @Field("response") int response);

}
