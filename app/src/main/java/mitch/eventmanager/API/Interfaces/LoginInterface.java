package mitch.eventmanager.API.Interfaces;

import mitch.eventmanager.API.Response.LoginResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Mitchell on 20/08/2017.
 */

public interface LoginInterface {

    @Headers("Content-Type:application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> login(@Field("enail") String email, @Field("password") String password);

    @Headers("Content-Type:application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> socialLogin(@Field("socialNetwork") String socialNetwork,
                                    @Field("socialLogin") String socialLogin,
                                    @Field("email") String email,
                                    @Field("socialUserId") String userID);

}
