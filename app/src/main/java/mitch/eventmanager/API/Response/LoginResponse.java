package mitch.eventmanager.API.Response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mitchell on 20/08/2017.
 */

public class LoginResponse {

    @SerializedName("success")
    public boolean success;

    @SerializedName("message")
    public String message;

    @SerializedName("token")
    public String token;

    @SerializedName("expiryDate")
    public String expiryDate;

    @SerializedName("usertype")
    public String userType;

    public LoginResponse() {

    }


    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public String getToken() {
        return token;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public String getUserType() {
        return userType;
    }


}
