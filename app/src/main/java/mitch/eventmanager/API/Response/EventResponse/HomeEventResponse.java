package mitch.eventmanager.API.Response.EventResponse;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import mitch.eventmanager.Event.Holder.HomeEvent;

/**
 * Created by paulmitchell on 27/08/2017.
 */

public class HomeEventResponse {

    @SerializedName("success")
    private boolean success;

    @SerializedName("message")
    private String message;

    @SerializedName("count")
    private int count;

    @Nullable
    @SerializedName("events")
    private List<HomeEvent> events;

    public HomeEventResponse() {
    }

    public HomeEventResponse(boolean success, List<HomeEvent> events) {
        this.success = success;
        this.events = events;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<HomeEvent> getEvents() {
        return events;
    }

    public void setEvents(List<HomeEvent> events) {
        this.events = events;
    }
}
