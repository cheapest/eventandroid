package mitch.eventmanager.API.Response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mitchell on 24/08/2017.
 */

public class SimpleResponse {

    @SerializedName("success")
    public boolean success;

    @SerializedName("message")
    public String message;

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return this.message;
    }

}
