package mitch.eventmanager.API.Response.EventResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import mitch.eventmanager.Event.EventGetPojo.Event;

/**
 * Created by Mitchell on 26/08/2017.
 */

// {"success":true,"event":{"eventInfo":{"eventID":"bfalrv65DTMV1Al9wuWOAJ6MOksSkT31yFjXKg0QWcDFAlDdmM8lKoIUxv7BGOrA","eventName":"Test","eventDescription":"Test","eventType":"Pub Crawl","isPublic":"0","guestInvite":"1","created":"2017-08-26","startTime":"2016-12-01 09:25:00","endTime":null,"secretKey":"def00000db468a27f5aedb0453f401057008fce6f330df0472f057db9fea4b20a19d9e3a08345a8d6f1ee28fc1a113823313cf66132a37be25d24ab118b6903f983da244"},"inviteInfo":{"inviteResponses":{"going":1,"notGoing":0,"maybeGoing":0}},"venueInfo":{"count":2,"venueList":[{"venueNickname":"the moon under water","arrival":"2017-04-07 08:42:00","venuePosition":"0","venueDescription":"Test Description","venueID":"2bqmA8tF3xVvEIDsyXIuzNccrWn8YESwB0zO0b9CNhpHAWh2T5ZjkosPSADg5yUu","address":{"placeName":"The Moon Under Water","address":"28 Leicester Square, London WC2H 7LE, UK","city":"London","region":"","postcode":"WC2H 7JY","country":"United Kingdom","longitude":"-0.12937459","latitude":"51.51027679"},"responses":{"generalInvite":[{"response":"3","socialUser":{"socialNetwork":"Facebook","socialID":"10153289776796269"}}],"userResponse":"3"}},{"venueNickname":"My House","arrival":"2017-03-09 08:42:00","venuePosition":"1","venueDescription":"Test","venueID":"sQbWZYZL7s24DFf3jTHDsbnQdKsE6uvRbcF1Ots3IR1Yo36e722c8HK5efQehycR","address":{"placeName":"95 S Park Cres","address":"95 S Park Cres, London SE6 1JL, UK","city":"London","region":"","postcode":"SE6 1JL","country":"United Kingdom","longitude":"0.00794080","latitude":"51.44077301"},"responses":{"generalInvite":[{"response":"3","socialUser":{"socialNetwork":"Facebook","socialID":"10153289776796269"}}],"userResponse":"3"}}]}}}

public class EventResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("event")
    @Expose
    private Event event;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
