package mitch.eventmanager.API.Request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import mitch.eventmanager.Event.Venue.Venue;

/**
 * Created by Mitchell on 24/08/2017.
 */

public class EventModifyRequest {

    @SerializedName("eventName")
    private String eventName;

    @SerializedName("eventPublic")
    private boolean eventPublic;

    @SerializedName("eventDescription")
    private String eventDescription;

    @SerializedName("eventGuestInvite")
    private boolean eventGuestInvite;

    @SerializedName("eventStart")
    private String eventStart;

    @SerializedName("eventType")
    private String eventType;

    @SerializedName("eventEnd")
    private String eventEnd;

    @SerializedName("eventVenues")
    private List<Venue> eventVenues;

    public EventModifyRequest() {

    }

    public EventModifyRequest(String eventName, boolean eventPublic, String eventDescription,
                              boolean eventGuestInvite, String eventStart, String eventType, String eventEnd, List<Venue> eventVenues) {
        this.eventName = eventName;
        this.eventPublic = eventPublic;
        this.eventDescription = eventDescription;
        this.eventGuestInvite = eventGuestInvite;
        this.eventStart = eventStart;
        this.eventType = eventType;
        this.eventEnd = eventEnd;
        this.eventVenues = eventVenues;
    }


    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public boolean isEventPublic() {
        return eventPublic;
    }

    public void setEventPublic(boolean eventPublic) {
        this.eventPublic = eventPublic;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public boolean isEventGuestInvite() {
        return eventGuestInvite;
    }

    public void setEventGuestInvite(boolean eventGuestInvite) {
        this.eventGuestInvite = eventGuestInvite;
    }

    public String getEventStart() {
        return eventStart;
    }

    public void setEventStart(String eventStart) {
        this.eventStart = eventStart;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventEnd() {
        return eventEnd;
    }

    public void setEventEnd(String eventEnd) {
        this.eventEnd = eventEnd;
    }

    public List<Venue> getEventVenues() {
        return eventVenues;
    }

    public void setEventVenues(List<Venue> eventVenues) {
        this.eventVenues = eventVenues;
    }
}
