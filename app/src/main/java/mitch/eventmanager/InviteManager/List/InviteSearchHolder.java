package mitch.eventmanager.InviteManager.List;

public class InviteSearchHolder {

    private String username;
    private String email;
    private String socialUserID;
    private String profilePic;
    private String simpleUserID;
    private boolean emptyResult;

    public InviteSearchHolder(boolean empty) {
        this.emptyResult = empty;
    }

    public InviteSearchHolder(String username, String email, String socialUserID, String profilePic) {
        this.username = username;
        this.email = email;
        this.socialUserID = socialUserID;
        this.profilePic = profilePic;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getSocialUserID() {
        return socialUserID;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public String getSimpleUserID() {
        return simpleUserID;
    }

    public boolean isEmptyResult() {
        return emptyResult;
    }

}
