package mitch.eventmanager.InviteManager.List;

/**
 * Created by paulmitchell on 01/05/2017.
 */

interface AdapterCallback {

    String onClick();

}
