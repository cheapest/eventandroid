package mitch.eventmanager.InviteManager.List;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import mitch.eventmanager.R;


/**
 * Created by Mitchell on 09/03/2016.
 */
public class InviteList extends
        RecyclerView.Adapter<InviteList.ViewHolder> {

    private ArrayList<InviteSearchHolder> eventList;
    private Context con;
    private int lastPosition = -1;


    public InviteList(Context context) {
        this.eventList = new ArrayList<>();
        this.con = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.invite_search_item, viewGroup, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        InviteSearchHolder vh = eventList.get(position);
        setAnimation(holder.itemView, position);

        holder.userID = (vh.getSimpleUserID().equals("")) ? vh.getSocialUserID() : vh.getSimpleUserID();

        holder.userName.setText(vh.getUsername());

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }


    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(con, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }


    @Override
    public int getItemCount() {
        return this.eventList.size();
    }

    public void removeItem(int item) {
        eventList.remove(item);
        notifyDataSetChanged();
    }

    public void emptyList() {
        if (getItemCount() == 0) {
            for (int x = 0; x < eventList.size(); x++) {
                removeItem(x);
            }
        }
    }

    public void insert(InviteSearchHolder data) {
        checkFirstElement();
        int nextItem = getItemCount();
        eventList.add(nextItem, data);
        notifyItemInserted(nextItem);
    }

    private void checkFirstElement() {
        if (!eventList.isEmpty() && (eventList.get(0).isEmptyResult())) {
            removeItem(0);
        }
    }

    public ArrayList<InviteSearchHolder> getEvents() {
        return this.eventList;
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView userName;
        private ImageView profilePic;
        private LinearLayout container;
        private String userID;

        ViewHolder(View itemView) {
            super(itemView);
            userName = (TextView) itemView.findViewById(R.id.invite_search_username);
            profilePic = (ImageView) itemView.findViewById(R.id.invite_search_profile_pic);
            container = (LinearLayout) itemView.findViewById(R.id.invite_search_container);
        }

    }
}