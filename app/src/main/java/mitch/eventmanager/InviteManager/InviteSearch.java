package mitch.eventmanager.InviteManager;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;

import org.json.JSONArray;

import butterknife.BindView;
import mitch.eventmanager.BaseActivity;
import mitch.eventmanager.InviteManager.List.InviteList;
import mitch.eventmanager.R;

public class InviteSearch extends BaseActivity {

    @BindView(R.id.quick_invite_list)
    RecyclerView quickAddList;
    @BindView(R.id.main_invite_list)
    RecyclerView inviteList;
    @BindView(R.id.send_non_user_invite_container)
    LinearLayout sendInviteContainer;
    @BindView(R.id.invite_search)
    EditText inviteSearchInput;


    private InviteList quickInviteListAdapter;
    private InviteList inviteListAdapter;


    @SuppressLint("MissingSuperCall")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_invite_search);

        setupViews();

        facebookFriendList();

    }

    private void facebookFriendList() {
        AccessToken token = AccessToken.getCurrentAccessToken();
        GraphRequest graphRequest = GraphRequest.newMyFriendsRequest(token, new GraphRequest.GraphJSONArrayCallback() {
            @Override
            public void onCompleted(JSONArray jsonArray, GraphResponse graphResponse) {
                Toast.makeText(InviteSearch.this, "" + jsonArray.length(), Toast.LENGTH_SHORT).show();
            }
        });
        graphRequest.executeAsync();
    }

    private void resetLists() {
        inviteListAdapter.emptyList();
        quickInviteListAdapter.emptyList();
    }

    private void setupViews() {

        quickInviteListAdapter = new InviteList(this);
        LinearLayoutManager quickLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        quickAddList.setLayoutManager(quickLayoutManager);
        quickAddList.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                quickLayoutManager.getOrientation());
        quickAddList.addItemDecoration(dividerItemDecoration);
        quickAddList.setAdapter(quickInviteListAdapter);


        /*inviteListAdapter = new InviteList(this);
        LinearLayoutManager inviteLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        inviteList.setLayoutManager(quickLayoutManager);
        inviteList.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration inviteDividerItemDecoration = new DividerItemDecoration(this,
                inviteLayoutManager.getOrientation());
        inviteList.addItemDecoration(inviteDividerItemDecoration);
        inviteList.setAdapter(inviteListAdapter);
*/
    }


    public void showAppInvite(View v) {

        String appLink = "https://fb.me/1883653855232597";

        CallbackManager sCallbackManager = CallbackManager.Factory.create();

        if (AppInviteDialog.canShow()) {
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(appLink)
                    .setPreviewImageUrl("")
                    .build();

            AppInviteDialog appInviteDialog = new AppInviteDialog(this);
            appInviteDialog.registerCallback(sCallbackManager,
                    new FacebookCallback<AppInviteDialog.Result>() {
                        @Override
                        public void onSuccess(AppInviteDialog.Result result) {
                            Log.d("Invitation", "Invitation Sent Successfully");
                            finish();
                        }

                        @Override
                        public void onCancel() {
                        }

                        @Override
                        public void onError(FacebookException e) {
                            Log.d("Invitation", "Error Occured");
                        }
                    });

            appInviteDialog.show(content);

        }

    }


}
