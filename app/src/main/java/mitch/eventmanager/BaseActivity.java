package mitch.eventmanager;

import android.app.ActivityManager;
import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import mitch.eventmanager.Event.EventModify;
import mitch.eventmanager.Misc.PermissionRequest;
import mitch.eventmanager.Misc.SessionManager;
import mitch.eventmanager.Misc.Snackbox;


public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    protected String URL = "https://event.thecheapestpint.co.uk/api/";

    protected boolean isLoggedin;


    protected int userType;
    protected String token;
    protected String expiryDate;

    protected Dialog dialog;

    protected Snackbox snackbox;

    protected SessionManager sm;
    protected double mLongitude;
    protected double mLatitude;
    protected Service locationService;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    // Location Stuff
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mLastLocation;
    private NavigationView navView;
    private DrawerLayout drawerLayout;

    protected void onCreate(Bundle savedInstanceState, int layout) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(layout);
        ButterKnife.bind(this);

        snackbox = new Snackbox(this);

        dialog = new Dialog(this);
        sm = new SessionManager(this); //initialise the session manager

        checkLoggedIn(); //lets check if the user is logged in now
        setupToolbar(); //set that toolbar up
        checkPermissions();
        // setupLocationManager();
    }

    protected void setupToolbar() {

        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        //Don't run if the class is Login
        if (isLoggedin && (!getClass().getSimpleName().equals("Login"))) {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = findViewById(R.id.navigation);
            navigationView.setNavigationItemSelectedListener(this);
            drawer.addDrawerListener(toggle);
        }
    }

    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PermissionRequest pr = new PermissionRequest(this);
            pr.checkPermissions();
        }
    }

    private void checkLoggedIn() {
        sm.checkLoginSession();
        isLoggedin = sm.isLoggedIn();
        token = sm.getToken();
        expiryDate = sm.getExpiry();
        if (sm.getUserType() != null) {
            userType = Integer.parseInt(sm.getUserType());
        }

    }

    public String getAuthHeader() {
        if (isLoggedin) {
            return token;
        }
        return "";
    }
    protected boolean checkIsLoggedInFromServer(JSONObject r){

        try{
            boolean loginCheck = r.has("loggedIn");
            boolean expireCheck = r.has("expired");
            if (loginCheck && expireCheck) {
                if (!r.getBoolean("loggedIn") || r.getBoolean("expired")) {
                    logOut();
                }
            }
        } catch (JSONException je){
            Log.d("Logout", je.getMessage());
        }

        //assumes loggedin
        return true;
    }

    protected void logOut() {
        sm.logoutSession();
        disconnectFromFacebook();
        Intent i = new Intent(this, Login.class);
        startActivity(i);
        finish();
    }

    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }
        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                LoginManager.getInstance().logOut();
            }
        }).executeAsync();
    }

    /* NAVIGATION DRAWER */

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        switch (item.getItemId()) {

            case R.id.addEventNav:
                Intent i = new Intent(this, EventModify.class);
                startActivity(i);
                break;
            case R.id.loggedOutNav:
                logOut();
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void setupLocationManager() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (mLastLocation != null) {
                mLatitude = mLastLocation.getLatitude();
                mLongitude = mLastLocation.getLongitude();
            }

            mGoogleApiClient.connect();
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        // See if a last known location is found

        // Create the location request
        long FASTEST_INTERVAL = 2000;
        long UPDATE_INTERVAL = 10 * 1000;
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        // Permission check is done in the base activity.
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("Location Error: ", connectionResult.getErrorMessage());
    }

    @Override
    public void onLocationChanged(Location location) {
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        Log.d("Location Update: ", msg);
        // You can now create a LatLng Object for use with maps
        mLongitude = location.getLongitude();
        mLatitude = location.getLatitude();
    }


}
