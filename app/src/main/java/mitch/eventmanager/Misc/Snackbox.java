package mitch.eventmanager.Misc;

import android.app.Activity;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;

import mitch.eventmanager.R;

/**
 * Created by Mitchell on 24/05/2016.
 */

public class Snackbox {
    private CoordinatorLayout layout;

    public Snackbox(Activity act){
        layout = (CoordinatorLayout) act.findViewById(R.id.coordinatorLayout);
    }

    public void showMessage(String title , String message) {
        Snackbar snackbar = Snackbar
                .make(layout, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

}
