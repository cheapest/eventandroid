package mitch.eventmanager.Misc;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.scottyab.aescrypt.AESCrypt;

import java.security.GeneralSecurityException;

import javax.inject.Inject;

import mitch.eventmanager.Config.Preferences;


public class SharedEditor {
    private Context con;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;

    @Inject
    public SharedEditor(Context c) {

        con = c;
        sp = PreferenceManager.getDefaultSharedPreferences(c);
        editor = sp.edit();
    }

    public void setValue(String var, String val) {
        try {
            String encrypted = AESCrypt.encrypt(Preferences.password, val);
            editor.putString(var, encrypted);
            commitEdit();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
    }

    public String getValue(String var) {
        String encrypted = sp.getString(var, "");
        String decrypted = "";
        try {
            decrypted = AESCrypt.decrypt(Preferences.password, encrypted);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        return decrypted;
    }

    public boolean getBoolean(String var){
        return sp.getBoolean(var, false);
    }


    public void setBoolean(String var, boolean val){
        editor.putBoolean(var, val);
        commitEdit();
    }


    public void emptyValue(String var){
        editor.remove(var);
        commitEdit();
    }

    private void commitEdit() {
        editor.commit();
    }


}
