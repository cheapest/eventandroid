package mitch.eventmanager.Misc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import dagger.Module;

@Module
public class DateTime {

    @Inject
    public DateTime(){

    }

    private static String getDateTime() {
        return initSDF().format(new Date());
    }

    private static SimpleDateFormat initSDF(){
        return new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
    }

    public static Date stringToDate(String date){
        Date d = null;
        try {
            d = initSDF().parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    public static String addHours(Date d, int h){
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(Calendar.HOUR, h);
        return initSDF().format(cal.getTime());
    }

    public static String addDays(Date d, int days){
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(Calendar.HOUR, (days * 60 * 60 * 24));
        return initSDF().format(cal.getTime());
    }

    public static String addMonths(Date d, int m){
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(Calendar.MONTH, m);
        return initSDF().format(cal.getTime());
    }

    public static String addYear(Date d, int year){
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(Calendar.YEAR, year);
        return initSDF().format(cal.getTime());
    }

    public static boolean compareDates(String dateCompare) {

        Date current;
        boolean flag = false;

        try {
            SimpleDateFormat sdf = initSDF();
            String currentString = getCurrentDate();
            current = sdf.parse(currentString);
            Date toCompare = sdf.parse(dateCompare);

            //Check if toCompare is before current
            flag = !toCompare.after(current);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return flag;
    }

    public static String getCurrentDate() {
        SimpleDateFormat currentDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.UK);
        return currentDate.format(new Date());
    }

    public boolean dateAfter(String date1, String date2) {

        boolean flag = false;
        try {
            SimpleDateFormat sdf = initSDF();
            Date d1 = sdf.parse(date1);
            Date d2 = sdf.parse(date2);
            //Check if date 1 is after date 2
            flag = d1.after(d2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return flag;
    }

}
