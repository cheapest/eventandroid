package mitch.eventmanager.Misc;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

/**
 * Created by Mitchell on 05/05/2016.
 */
public class PermissionRequest {

    Context mContext;
    public PermissionRequest(Context con){
        mContext = con;
    }

    public void checkPermissions(){
        checkFineLocation();
    }

    private void checkFineLocation(){
        if ((android.support.v4.app.ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) ||
                (android.support.v4.app.ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED)) {
            // Check Permissions Nowprivate static final int REQUEST_LOCATION = 2;

            if (android.support.v4.app.ActivityCompat.shouldShowRequestPermissionRationale((Activity) mContext,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Display UI and wait for user interaction
            } else {
                android.support.v4.app.ActivityCompat.requestPermissions(
                        (Activity) mContext, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION},
                        2);
            }
        }
    }



}
