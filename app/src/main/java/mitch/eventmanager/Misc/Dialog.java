package mitch.eventmanager.Misc;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by Paul on 19/10/2014.
 */
public class Dialog {
    Context ctx;
    public Dialog(Context c){
        // gets the context
        ctx = c;
    }

    public boolean showMessage(String title, String message){
        new AlertDialog.Builder(ctx)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
        return true;
    }

}