package mitch.eventmanager.Misc;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mitchell on 10/02/2016.
 */
public class Validation {

    private static final String emailRegex =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String usernameRegex =
            "[a-zA-Z0-9]{3,}";
    private Pattern pattern;
    private Matcher matcher;
    private String email;
    private Map<String, String> result;



    public Validation(){
        result = new HashMap<>();
    }

    public Map checkEmail(String email){
        this.email = email;
        if(this.checkEmptyString()){
            result.put("success","false");
            result.put("message","No Email Was Found.");
        } else {

        }

        return result;
    }

    private boolean checkEmailRegex(){
        pattern = Pattern.compile(emailRegex);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private boolean checkEmptyString(){
        return email.isEmpty();
    }

}
