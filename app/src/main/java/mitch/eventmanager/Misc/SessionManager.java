package mitch.eventmanager.Misc;

import android.content.Context;

import java.util.Map;

import javax.inject.Inject;


public class SessionManager {


    @Inject
    SharedEditor se;
    private String token;
    private boolean isLoggedIn;
    private boolean isAdmin;
    private String userType;
    private boolean expired;

    public SessionManager(Context con) {
        se = new SharedEditor(con);
        isLoggedIn = false;
        isAdmin = false;
    }

    public void createLoginSession(Map<String,String> user){
        /*
            0 - token
            1 - usertype
            2 - expiry date
         */
        se.setValue("token",user.get("token"));
        se.setValue("userType",user.get("userType"));
        se.setValue("expiryDate",user.get("expiryDate"));
        se.setValue("socialLogin", user.get("socialLogin"));
        se.setValue("socialUserId", user.get("socialUserId"));
    }
    public void checkLoginSession(){
        token = se.getValue("token");
        if(!token.isEmpty()) {
            userType = se.getValue("userType");
            if (userType.equals("2")) {
                isAdmin = true;
            }
            String expiry = se.getValue("expiryDate");
            checkExpiry(expiry);
        }
    }
    private void checkExpiry(String expiry){
        if(!expiry.isEmpty()){
            boolean expired = DateTime.compareDates(expiry);
            if(expired){
                this.expired = true;
                logoutSession();
            } else {
                this.expired = false;
                isLoggedIn = true;
            }
        }
    }
    public String getUserType() {
        return userType;
    }
    public String getExpiry(){return se.getValue("expiryDate");}
    public String getToken() {
        return token;
    }
    public boolean isExpired() {
        return expired;
    }
    public boolean isLoggedIn() { return isLoggedIn; }
    public void logoutSession(){
        se.emptyValue("expiryDate");
        se.emptyValue("token");
        se.emptyValue("userType");
    }
}
