package mitch.eventmanager.Event;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.google.android.gms.maps.GoogleMap;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import mitch.eventmanager.API.APIClient;
import mitch.eventmanager.API.Interfaces.EventInterface;
import mitch.eventmanager.API.Request.EventModifyRequest;
import mitch.eventmanager.API.Response.SimpleResponse;
import mitch.eventmanager.BaseActivity;
import mitch.eventmanager.Event.List.VenueViewAdapter;
import mitch.eventmanager.Event.List.helper.OnStartDragListener;
import mitch.eventmanager.Event.Venue.Address;
import mitch.eventmanager.Event.Venue.Venue;
import mitch.eventmanager.Event.Venue.VenueComparator;
import mitch.eventmanager.Home;
import mitch.eventmanager.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventModify extends BaseActivity implements OnStartDragListener {

    @BindView(R.id.eventName)
    public EditText eventName;

    @BindView(R.id.eventDescription)
    public EditText eventDescription;

    @BindView(R.id.publicPrivateCheck)
    public CheckBox eventPubPriv;

    @BindView(R.id.guestInvite)
    public CheckBox guestInvite;

    @BindView(R.id.endDate)
    public Button endDateEdit;

    @BindView(R.id.endTime)
    public Button endTimeEdit;

    @BindView(R.id.eventSpinner)
    public Spinner eventType;

    private EventInterface eventInterface;
    private VenueViewAdapter vVA;

    private RecyclerView venueList;

    private ItemTouchHelper mItemTouchHelper;

    private GoogleMap googleMap;

    private boolean toUpdate = false;

    private String eventID;

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_event_add);
        venueList = findViewById(R.id.venueViewRecycler);

        setupForm();
        setupRecyclerList();

        Intent intent = getIntent();

        if (intent != null && intent.hasExtra("toUpdate") && intent.getBooleanExtra("toUpdate", false)) {
            toUpdate = true;
            if (intent.hasExtra("eventID") && intent.getStringExtra("eventID") != null) {
                eventID = intent.getStringExtra("eventID");

            }
        }

        eventInterface = APIClient.getClient(URL).create(EventInterface.class);

        setupEvent();
    }

    private void setupForm() {

        endDateEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endDate();
            }
        });
        endTimeEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTime();
            }
        });

        ArrayAdapter<String> types = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.event_type_list));
        eventType.setAdapter(types);
    }


    private void setupEvent() {
        EventModifyRequest event = getEventDetails();

        String eventName = event.getEventName();
        String eventDescription = event.getEventDescription();

        this.eventName.setText(eventName);
        this.eventDescription.setText(eventDescription);

        boolean pubPriv = event.isEventPublic();
        boolean guestInvite = event.isEventGuestInvite();

        this.eventPubPriv.setChecked(pubPriv);
        this.guestInvite.setChecked(guestInvite);

        List<Venue> venues = event.getEventVenues();
        for (Venue venue : venues) {

            vVA.insert(venue);
        }

    }

    private EventModifyRequest getEventDetails() {
        String url = URL + "";

        EventModifyRequest eventModifyRequest = new EventModifyRequest();
        eventModifyRequest.setEventName("TEST EVENT");
        eventModifyRequest.setEventDescription("Test Description");
        eventModifyRequest.setEventPublic(true);
        eventModifyRequest.setEventGuestInvite(false);

        Venue v = new Venue();
        v.setVenueNickname("Test Nickname");
        v.setVenueDescription("Test Description");
        v.setArrival("28/08/2017 03:00:00");
        v.setVenuePosition(0);
        Address address = new Address();
        address.setRegion("London");
        address.setAddress("95 South Park Crescent");
        address.setCity("London");
        address.setLatitude(51.4403080);
        address.setLongitude(0.008);
        address.setCountry("United Kingdom");
        address.setPlaceName("Test");
        address.setPostcode("SE6 1JL");
        v.setAddress(address);
        List<Venue> venues = new ArrayList<>();
        venues.add(v);
        eventModifyRequest.setEventVenues(venues);
        return eventModifyRequest;
    }

    private void setupRecyclerList() {
        vVA = new VenueViewAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        venueList.setLayoutManager(linearLayoutManager);
        venueList.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(venueList.getContext(),
                linearLayoutManager.getOrientation());
        venueList.addItemDecoration(dividerItemDecoration);

        //ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(vVA);
        //mItemTouchHelper = new ItemTouchHelper(callback);
        //mItemTouchHelper.attachToRecyclerView(venueList);
        venueList.setAdapter(vVA);


        if (toUpdate) {
            EventModifyRequest event = getEventDetails();
        } else {
            Venue venue = new Venue();
            venue.setVenueNickname("No Venues Found");
            vVA.insert(venue);
        }
    }

    public void addEventVenue() {
        Intent i = new Intent(this, EventAddForm.class);
        startActivityForResult(i, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Venue venue = (Venue) data.getSerializableExtra("venue");
                vVA.insert(venue);
            }
        }
    }//onActivityResult

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.event_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.addVenue:
                addEventVenue();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void endDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        endDateEdit.setText(checkAndAppend(dayOfMonth + 1) + "/" + checkAndAppend(monthOfYear) + "/" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private String checkAndAppend(int check) {
        String appended;
        if (check < 10) {
            appended = "0" + check;
        } else {
            appended = "" + check;
        }

        return appended;
    }

    public void endTime() {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR);
        int mMinute = c.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                endTimeEdit.setText(String.format("%s:%s", checkAndAppend(selectedHour), checkAndAppend(selectedMinute)));
            }
        }, mHour, mMinute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

    public void addEvent(View view) {
        ArrayList<Venue> list = vVA.getVenues();
        if (eventName.getText().toString().equals("")) {
            snackbox.showMessage("", "You need to enter an event name.");
        } else if (endDateEdit.getText().toString().equals("") && !endTimeEdit.getText().toString().equals("")) {
            endDateEdit.setError("Select A Date.");
            snackbox.showMessage("", "We need a date to go with your end time.");
        } else if ((list.size() == 0) || (list.get(0).getVenueNickname().equals("No Venues Found."))) {
            snackbox.showMessage("", "You need to add a venue.");
        } else {
            approveSubmit();
        }
    }

    private void approveSubmit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Are You Sure You Wish To Create This Event?");
        builder.setMessage("");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                addTheEvent();
            }
        });
        builder.setNegativeButton("No", null);
        builder.show();
    }

    private void addTheEvent() {
        Gson convert = new Gson();
        convert.serializeNulls();

        String name = eventName.getText().toString();
        String description = eventDescription.getText().toString();
        boolean eventPublicPrivate = eventPubPriv.isChecked();
        boolean guestCanInvite = guestInvite.isChecked();
        String endDateTime;
        if (!endDateEdit.getText().toString().equals("") && endTimeEdit.getText().toString().equals("")) {
            //Date given and time not. Default the time to 23:59
            endDateTime = endDateEdit.getText().toString() + " 23:59";
        } else {
            endDateTime = endDateEdit.getText().toString() + " " + endTimeEdit.getText().toString();
        }

        ArrayList<Venue> venues = vVA.getVenues();
        //Lets sort the venues
        Collections.sort(venues, new VenueComparator());
        String startDateTime = venues.get(0).getArrival();

        for (int x = 0; x < venues.size(); x++) {
            venues.get(x).setVenuePosition(x);
        }

        //public EventModifyRequest(String eventName, boolean eventPublic, String eventDescription,
        //boolean eventGuestInvite, String eventStart, String eventType, String eventEnd, List<Venue> eventVenues) {

        EventModifyRequest event = new EventModifyRequest(
                name, eventPublicPrivate, description, guestCanInvite, startDateTime,
                eventType.getSelectedItem().toString(), endDateTime, venues
        );

        sendToServer(event);

    }

    private void sendToServer(EventModifyRequest event) {

        Call<SimpleResponse> addEvent = eventInterface.addEvent(getAuthHeader(), event);

        addEvent.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {

                if (!response.isSuccessful()) {

                    Log.e("ERROR", "NOT SUCCESSFUL");

                } else {

                    if (!response.body().isSuccess()) {
                        snackbox.showMessage("", response.body().getMessage());
                    } else {
                        displaySuccessMessage(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                snackbox.showMessage("Error Result", t.getMessage());
            }
        });

    }


    private void displaySuccessMessage(String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Event Created!");
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                sendToMain();
            }
        });
        builder.show();
    }

    private void sendToMain() {
        Intent i = new Intent(this, Home.class);
        startActivity(i);
        finish();
    }
}
