package mitch.eventmanager.Event.Holder;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mitchell on 18/12/2016.
 */

public class HomeEvent {

    @SerializedName("eventName")
    private String eventName;

    @SerializedName("imageURL")
    private String imageURL;

    private Boolean emptyVenue;

    @SerializedName("eventID")
    private String eventID;

    @SerializedName("startTime")
    private String startTime;

    @SerializedName("endTime")
    private String endTime;


    public HomeEvent(Boolean empty){
        this.emptyVenue = empty;
    }

    public HomeEvent(String eventName, String imageURL, Boolean emptyVenue, String eventID, String startTime, String endTime) {
        this.eventName = eventName;
        this.imageURL = imageURL;
        this.emptyVenue = emptyVenue;
        this.eventID = eventID;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Boolean getEmptyVenue() {
        return emptyVenue;
    }

    public void setEmptyVenue(Boolean emptyVenue) {
        this.emptyVenue = emptyVenue;
    }

    public String getEventID() {
        return eventID;
    }

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
