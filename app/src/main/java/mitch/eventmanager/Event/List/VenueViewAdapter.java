package mitch.eventmanager.Event.List;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import mitch.eventmanager.Event.Venue.Venue;
import mitch.eventmanager.R;


/**
 * Created by Mitchell on 09/03/2016.
 */
public class VenueViewAdapter extends
        RecyclerView.Adapter<VenueViewAdapter.ViewHolder> {

    private ArrayList<Venue> venueList;
    private Context con;
    //private OnStartDragListener dragListener;

    public VenueViewAdapter(Context context) {
        this.venueList = new ArrayList<>();
        this.con = context;
        //this.dragListener = (OnStartDragListener) context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.event_card_small, viewGroup, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Venue vh = venueList.get(position);
        if (vh.getVenueNickname() == null || vh.getVenueNickname().equals("")) {
            holder.venueName.setText(vh.getAddress().getPlaceName());
        } else {
            holder.venueName.setText(vh.getVenueNickname());
        }

      /*  holder.card.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                    dragListener.onStartDrag(holder);
                }
                return false;
            }
        });
        */
    }

    @Override
    public int getItemCount() {
        return this.venueList.size();
    }

    public void removeItem(int item) {
        venueList.remove(item);
        //notifyItemChanged(item);
        notifyDataSetChanged();
    }

    public void insert(Venue data) {
        if (venueList.size() == 1 && venueList.get(0).isEmpty()) {
            this.venueList.clear();
        }
        int nextItem = getItemCount();
        venueList.add(nextItem, data);
        notifyItemInserted(nextItem);
    }

    public ArrayList<Venue> getVenues() {
        return this.venueList;
    }

    public void updateVenueCheck(final String id, final String name) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this.con);
        alertDialogBuilder.setMessage("Are you sure you wish to update this Venue: " + name + "?");

        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //do nothing
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /*@Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(venueList, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {

    }
*/

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView venueName;
        ImageButton conButton;
        CardView card;

        ViewHolder(View itemView) {
            super(itemView);
            venueName = itemView.findViewById(R.id.venue_name);
            conButton = itemView.findViewById(R.id.contextUpdateDelete);
            card = itemView.findViewById(R.id.event_small_card);

            itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                    menu.add("Update").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            //String name = drinkNameLabel.getText().toString();
                            //updateDrinkCheck(hiddenID, name);
                            return true;
                        }
                    });

                    menu.add("Delete").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            //String name = drinkNameLabel.getText().toString();
                            //removeCheck(name);
                            return true;
                        }
                    });

                }
            });

            conButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.showContextMenu();
                }
            });

        }

        /*
        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }

        */
    }
}