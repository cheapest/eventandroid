package mitch.eventmanager.Event.List;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import mitch.eventmanager.Event.Venue.Venue;
import mitch.eventmanager.Event.Venue.VenueDetail;
import mitch.eventmanager.R;


public class VenueTimelineAdapter extends
        RecyclerView.Adapter<VenueTimelineAdapter.ViewHolder> {

    private ArrayList<Venue> venueList;
    private Context con;
    private String token;
    //private OnStartDragListener dragListener;

    public VenueTimelineAdapter(Context context) {
        this.venueList = new ArrayList<>();
        this.con = context;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public VenueTimelineAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.venue_timeline, viewGroup, false);

        return new VenueTimelineAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final VenueTimelineAdapter.ViewHolder holder, int position) {
        final Venue vh = venueList.get(position);
        if (vh.getVenueNickname() == null || vh.getVenueNickname().equals("")) {
            holder.venueName.setText(vh.getAddress().getPlaceName());
        } else {
            holder.venueName.setText(vh.getVenueNickname());
        }

        holder.venuePosition.setText(Integer.toString(vh.getVenuePosition() + 1));
        holder.venueAddress.setText(vh.getAddress().getAddress());

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(con, VenueDetail.class);
                i.putExtra("venue", vh);
                i.putExtra("token", token);
                con.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.venueList.size();
    }

    public void insert(Venue data) {
        int nextItem = getItemCount();
        venueList.add(nextItem, data);
        notifyItemInserted(nextItem);
    }

    public ArrayList<Venue> getVenues() {
        return this.venueList;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView venuePosition;
        TextView venueName;
        TextView venueAddress;
        CardView card;

        ViewHolder(View itemView) {
            super(itemView);
            venuePosition = itemView.findViewById(R.id.venue_position);
            venueName = itemView.findViewById(R.id.venue_nickname);
            venueAddress = itemView.findViewById(R.id.venue_list_location);
            card = itemView.findViewById(R.id.venue_timeline_card);
        }

    }
}