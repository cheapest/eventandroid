package mitch.eventmanager.Event.List;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import mitch.eventmanager.Event.EventController;
import mitch.eventmanager.Event.Holder.HomeEvent;
import mitch.eventmanager.R;


/**
 * Created by Mitchell on 09/03/2016.
 */
public class HomePageAdapter extends
        RecyclerView.Adapter<HomePageAdapter.ViewHolder> {

    private ArrayList<HomeEvent> eventList;
    private Context con;
    private int lastPosition = -1;


    public HomePageAdapter(Context context) {
        this.eventList = new ArrayList<>();
        this.con = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.event_card_home, viewGroup, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        HomeEvent vh = eventList.get(position);

        if(vh.getEmptyVenue()){
            holder.eventName.setText(R.string.no_event_string);
        } else {
            holder.eventName.setText(vh.getEventName());
            holder.eventID = vh.getEventID();
            //set images at a later date.

            //set onclick listener

            holder.cardHolder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, EventController.class);
                    i.putExtra("eventID", holder.eventID);
                    con.startActivity(i);
                }
            });
        }

        holder.noEvents = vh.getEmptyVenue();
        setAnimation(holder.itemView, position);

    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(con, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }


    @Override
    public int getItemCount() {
        return this.eventList.size();
    }

    public void removeItem(int item) {
        eventList.remove(item);
        notifyDataSetChanged();
    }

    public void insert(HomeEvent data) {
        checkFirstElement();

        int nextItem = getItemCount();
        eventList.add(nextItem, data);
        notifyItemInserted(nextItem);
    }

    private void checkFirstElement(){
        if(!eventList.isEmpty() && eventList.get(0).getEmptyVenue()){
            removeItem(0);
        }
    }

    public ArrayList<HomeEvent> getEvents() {
        return this.eventList;
    }


    class ViewHolder extends RecyclerView.ViewHolder{

        private TextView eventName;
        private ImageView eventLogo;
        private Boolean noEvents;
        private String eventID;
        private CardView cardHolder;

         ViewHolder(View itemView) {
            super(itemView);
            eventName = (TextView) itemView.findViewById(R.id.eventTitle);
            cardHolder = (CardView) itemView.findViewById(R.id.eventCardSmall);
            eventLogo = (ImageView) itemView.findViewById(R.id.eventPhoto);
        }

    }
}