package mitch.eventmanager.Event.EventGetPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GeneralInvite {

    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("socialUser")
    @Expose
    private SocialUser socialUser;

    /**
     * No args constructor for use in serialization
     */
    public GeneralInvite() {
    }

    /**
     * @param socialUser
     * @param response
     */
    public GeneralInvite(String response, SocialUser socialUser) {
        super();
        this.response = response;
        this.socialUser = socialUser;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public SocialUser getSocialUser() {
        return socialUser;
    }

    public void setSocialUser(SocialUser socialUser) {
        this.socialUser = socialUser;
    }

}