package mitch.eventmanager.Event.EventGetPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by Mitchell on 26/08/2017.
 */

public class Event {

    @SerializedName("eventInfo")
    @Expose
    private EventInfo eventInfo;
    @SerializedName("inviteInfo")
    @Expose
    private InviteInfo inviteInfo;
    @SerializedName("venueInfo")
    @Expose
    private VenueInfo venueInfo;

    /**
     * No args constructor for use in serialization
     */
    public Event() {
    }

    public Event(EventInfo eventInfo, InviteInfo inviteInfo, VenueInfo venueInfo) {
        super();
        this.eventInfo = eventInfo;
        this.inviteInfo = inviteInfo;
        this.venueInfo = venueInfo;
    }

    public EventInfo getEventInfo() {
        return eventInfo;
    }

    public void setEventInfo(EventInfo eventInfo) {
        this.eventInfo = eventInfo;
    }

    public InviteInfo getInviteInfo() {
        return inviteInfo;
    }

    public void setInviteInfo(InviteInfo inviteInfo) {
        this.inviteInfo = inviteInfo;
    }

    public VenueInfo getVenueInfo() {
        return venueInfo;
    }

    public void setVenueInfo(VenueInfo venueInfo) {
        this.venueInfo = venueInfo;
    }
}
