package mitch.eventmanager.Event.EventGetPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InviteInfo {

    @SerializedName("inviteResponses")
    @Expose
    private InviteResponses inviteResponses;

    /**
     * No args constructor for use in serialization
     */
    public InviteInfo() {
    }

    /**
     * @param inviteResponses
     */
    public InviteInfo(InviteResponses inviteResponses) {
        super();
        this.inviteResponses = inviteResponses;
    }

    public InviteResponses getInviteResponses() {
        return inviteResponses;
    }

    public void setInviteResponses(InviteResponses inviteResponses) {
        this.inviteResponses = inviteResponses;
    }

}

