package mitch.eventmanager.Event.EventGetPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InviteResponses {

    @SerializedName("going")
    @Expose
    private Integer going;
    @SerializedName("notGoing")
    @Expose
    private Integer notGoing;
    @SerializedName("maybeGoing")
    @Expose
    private Integer maybeGoing;

    /**
     * No args constructor for use in serialization
     */
    public InviteResponses() {
    }

    /**
     * @param going
     * @param maybeGoing
     * @param notGoing
     */
    public InviteResponses(Integer going, Integer notGoing, Integer maybeGoing) {
        super();
        this.going = going;
        this.notGoing = notGoing;
        this.maybeGoing = maybeGoing;
    }

    public Integer getGoing() {
        return going;
    }

    public void setGoing(Integer going) {
        this.going = going;
    }

    public Integer getNotGoing() {
        return notGoing;
    }

    public void setNotGoing(Integer notGoing) {
        this.notGoing = notGoing;
    }

    public Integer getMaybeGoing() {
        return maybeGoing;
    }

    public void setMaybeGoing(Integer maybeGoing) {
        this.maybeGoing = maybeGoing;
    }

}