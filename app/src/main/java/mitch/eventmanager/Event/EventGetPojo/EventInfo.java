package mitch.eventmanager.Event.EventGetPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import mitch.eventmanager.Event.Venue.Venue;


public class EventInfo {


    @SerializedName("eventID")
    @Expose
    private String eventID;
    @SerializedName("eventName")
    @Expose
    private String eventName;
    @SerializedName("eventDescription")
    @Expose
    private String eventDescription;
    @SerializedName("eventType")
    @Expose
    private String eventType;
    @SerializedName("isPublic")
    @Expose
    private String isPublic;
    @SerializedName("guestInvite")
    @Expose
    private String guestInvite;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("endTime")
    @Expose
    private Object endTime;
    @SerializedName("secretKey")
    @Expose
    private String secretKey;
    @SerializedName("venues")
    @Expose
    private List<Venue> venues;

    /**
     * No args constructor for use in serialization
     */
    public EventInfo() {
    }

    public EventInfo(String eventID, String eventName, String eventDescription, String eventType, String isPublic, String guestInvite, String created, String startTime, Object endTime, String secretKey) {
        super();
        this.eventID = eventID;
        this.eventName = eventName;
        this.eventDescription = eventDescription;
        this.eventType = eventType;
        this.isPublic = isPublic;
        this.guestInvite = guestInvite;
        this.created = created;
        this.startTime = startTime;
        this.endTime = endTime;
        this.secretKey = secretKey;
    }

    public String getEventID() {
        return eventID;
    }

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(String isPublic) {
        this.isPublic = isPublic;
    }

    public String getGuestInvite() {
        return guestInvite;
    }

    public void setGuestInvite(String guestInvite) {
        this.guestInvite = guestInvite;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Object getEndTime() {
        return endTime;
    }

    public void setEndTime(Object endTime) {
        this.endTime = endTime;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public List<Venue> getVenues() {
        return this.venues;
    }

    public void setVenues(List<Venue> venues) {
        this.venues = venues;
    }

}
