package mitch.eventmanager.Event.EventGetPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Responses {

    @SerializedName("generalInvite")
    @Expose
    private List<GeneralInvite> generalInvite = null;
    @SerializedName("userResponse")
    @Expose
    private String userResponse;

    /**
     * No args constructor for use in serialization
     */
    public Responses() {
    }

    /**
     * @param userResponse
     * @param generalInvite
     */
    public Responses(List<GeneralInvite> generalInvite, String userResponse) {
        super();
        this.generalInvite = generalInvite;
        this.userResponse = userResponse;
    }

    public List<GeneralInvite> getGeneralInvite() {
        return generalInvite;
    }

    public void setGeneralInvite(List<GeneralInvite> generalInvite) {
        this.generalInvite = generalInvite;
    }

    public String getUserResponse() {
        return userResponse;
    }

    public void setUserResponse(String userResponse) {
        this.userResponse = userResponse;
    }

}