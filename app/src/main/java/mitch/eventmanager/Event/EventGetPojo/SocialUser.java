package mitch.eventmanager.Event.EventGetPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SocialUser {

    @SerializedName("socialNetwork")
    @Expose
    private String socialNetwork;
    @SerializedName("socialID")
    @Expose
    private String socialID;

    /**
     * No args constructor for use in serialization
     */
    public SocialUser() {
    }

    /**
     * @param socialID
     * @param socialNetwork
     */
    public SocialUser(String socialNetwork, String socialID) {
        super();
        this.socialNetwork = socialNetwork;
        this.socialID = socialID;
    }

    public String getSocialNetwork() {
        return socialNetwork;
    }

    public void setSocialNetwork(String socialNetwork) {
        this.socialNetwork = socialNetwork;
    }

    public String getSocialID() {
        return socialID;
    }

    public void setSocialID(String socialID) {
        this.socialID = socialID;
    }

}