package mitch.eventmanager.Event.EventGetPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import mitch.eventmanager.Event.Venue.Venue;

public class VenueInfo {

    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("venueList")
    @Expose
    private List<Venue> venueList = null;

    /**
     * No args constructor for use in serialization
     */
    public VenueInfo() {
    }

    /**
     * @param count
     * @param venueList
     */
    public VenueInfo(Integer count, List<Venue> venueList) {
        super();
        this.count = count;
        this.venueList = venueList;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<Venue> getVenueList() {
        return venueList;
    }

    public void setVenueList(List<Venue> venueList) {
        this.venueList = venueList;
    }

}