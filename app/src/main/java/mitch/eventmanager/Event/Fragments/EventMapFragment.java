package mitch.eventmanager.Event.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mitch.eventmanager.Event.EventGetPojo.VenueInfo;
import mitch.eventmanager.Event.Venue.Address;
import mitch.eventmanager.Event.Venue.Venue;
import mitch.eventmanager.Misc.Snackbox;
import mitch.eventmanager.R;


public class EventMapFragment extends Fragment implements OnMapReadyCallback {

    @BindView(R.id.map)
    public MapView mMapView;
    EventMapFragment myFrag;
    private GoogleMap mMap;
    private Double lon;
    private Double lat;

    private List<Venue> venueList;


    public EventMapFragment() {
        // Required empty public constructor
    }

    public static EventMapFragment newInstance() {
        return new EventMapFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            myFrag = EventMapFragment.newInstance();

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.event_page_venue_card, container, false);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(view);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setUserLocation(Double lat, Double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public void moveMap(LatLng coordinates) {
        if (mMap != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 14.0f));
        }
    }

    public void sendData(VenueInfo data) {

        if (mMap == null) {
            venueList = data.getVenueList();
        } else {
            addMarkers(data.getVenueList());
        }

    }

    public void addMarkers(List<Venue> data) {
        Snackbox snackbox = new Snackbox(getActivity());

        if (data == null || data.size() == 0) {

            snackbox.showMessage("Error", "No Venues Found...");

        } else {
            for (Venue venue : data) {
                setAddress(venue.getAddress());
            }
        }

    }

    private void addVenueMarker(LatLng venue, String placeName) {
        Marker marker = mMap.addMarker(new MarkerOptions()
                .position(venue)
                .title(placeName));
        marker.showInfoWindow();
        moveMap(venue);
    }


    private void setAddress(Address address) {
        LatLng coordinates = new LatLng(address.getLatitude(), address.getLongitude());
        addVenueMarker(coordinates, address.getPlaceName());
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);


    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (venueList != null) {
            addMarkers(venueList);
        }
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 14.0f));
    }

}
