package mitch.eventmanager.Event.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mitch.eventmanager.Event.EventGetPojo.Event;
import mitch.eventmanager.Event.EventGetPojo.EventInfo;
import mitch.eventmanager.Event.EventGetPojo.InviteInfo;
import mitch.eventmanager.Event.EventGetPojo.InviteResponses;
import mitch.eventmanager.Event.List.VenueTimelineAdapter;
import mitch.eventmanager.Event.Venue.Venue;
import mitch.eventmanager.R;

public class EventMainFragment extends Fragment {


    private static final String[] infoArgs = {"eventID", "eventName", "eventDescription", "eventType", "isPublic", "guestInvite", "created", "startTime", "endTime"};

    @BindView(R.id.event_name)
    public TextView eventName;

    @BindView(R.id.event_description)
    public TextView eventDescription;

    @BindView(R.id.event_type)
    public TextView eventType;

    @BindView(R.id.start_date_time)
    public TextView startDataTime;

    @BindView(R.id.end_date_image)
    public ImageView endImage;

    @BindView(R.id.end_date_time)
    public TextView endDataTime;

    @BindView(R.id.hosted_by)
    public TextView host;

    public VenueTimelineAdapter vVA;

    @BindView(R.id.response_spinner)
    public Spinner response;

    @BindView(R.id.not_going_value)
    public TextView notGoing;

    @BindView(R.id.maybe_going_value)
    public TextView maybeGoing;

    @BindView(R.id.going_value)
    public TextView going;


    private HashMap<String, Integer> userResponses;


    public EventMainFragment() {
        // Required empty public constructor
    }

    public static EventMainFragment newInstance() {
        return new EventMainFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.event_page_main_info_fragment, container, false);

        ButterKnife.bind(v);

        v = setupInviteView(v);
        v = setupRecyclerView(v);
        // Inflate the layout for this fragment

        userResponses = new HashMap<>();
        userResponses.put("1", 0);
        userResponses.put("2", 0);
        userResponses.put("3", 0);
        return v;
    }


    private View setupInviteView(View v) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(v.getContext(),
                android.R.layout.simple_spinner_item, getResources()
                .getStringArray(R.array.event_response_list)); //selected item will look like a spinner set from XML

        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        response.setAdapter(spinnerArrayAdapter);
        return v;
    }

    private View setupRecyclerView(View v) {
        RecyclerView venueList = v.findViewById(R.id.venueList);
        vVA = new VenueTimelineAdapter(getContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        venueList.setLayoutManager(linearLayoutManager);
        venueList.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(venueList.getContext(),
                linearLayoutManager.getOrientation());
        venueList.addItemDecoration(dividerItemDecoration);
        venueList.setAdapter(vVA);
        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void sendData(Event event, String token) {
        setupEventInfo(event.getEventInfo());
        setupVenueList(event.getVenueInfo().getVenueList(), token);
            //Use Venue List to determine overall user response.
        setupBasicInviteInfo(event.getInviteInfo());
    }

    private void setupEventInfo(EventInfo event) {
        eventName.setText(event.getEventName());
        eventDescription.setText(event.getEventDescription());
        eventType.setText(event.getEventType());

        startDataTime.setText(event.getStartTime());

        if (event.getEndTime().equals("") || event.getEndTime() == null) {
            endImage.setVisibility(View.GONE);
            endDataTime.setVisibility(View.GONE);
        }
    }

    private void setupBasicInviteInfo(InviteInfo inviteStats) {

        InviteResponses inviteResponses = inviteStats.getInviteResponses();

        going.setText(inviteResponses.getGoing());
        maybeGoing.setText(inviteResponses.getMaybeGoing());
        notGoing.setText(inviteResponses.getNotGoing());

        //setResponse();
    }

    private void setupVenueList(List<Venue> venueList, String token) {

        vVA.setToken(token);

        for (Venue v : venueList) {
            vVA.insert(v);
        }

    }


    private void findOverallResponse(JSONObject resp) {
        try {
            String userResp = resp.getString("userResponse");
            int newResp = userResponses.get(userResp) + 1;
            userResponses.put(userResp, newResp);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void getFacebookUserFriendList() {
        GraphRequest request = GraphRequest.newMyFriendsRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONArrayCallback() {
            @Override
            public void onCompleted(JSONArray objects, GraphResponse response) {
                try {

                    JSONObject json = response.getJSONObject();
                    json.getJSONObject("");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        request.executeAsync();
    }

    private void setResponse() {
        try {

            if (userResponses.get("3") > 0) {
                // Going is first in the list.
                // Need it like that for UX sake
                response.setSelection(0);
            } else if ((userResponses.get("3") == 0) && (userResponses.get("2") > 0)) {
                response.setSelection(1);
            } else if ((userResponses.get("3") == 0) && (userResponses.get("2") == 0) && (userResponses.get("1") > 0)) {
                response.setSelection(2);
            } else {
                response.setSelection(0);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
