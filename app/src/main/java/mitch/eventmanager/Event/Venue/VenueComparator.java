package mitch.eventmanager.Event.Venue;

import java.util.Comparator;

import mitch.eventmanager.Misc.DateTime;



public class VenueComparator implements Comparator<Venue> {
    @Override
    public int compare(Venue o1, Venue o2) {
        DateTime dt = new DateTime();
        boolean check = dt.dateAfter(o1.getArrival(), o2.getArrival());
        return check ? 1 : -1;
    }
}
