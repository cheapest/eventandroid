package mitch.eventmanager.Event.Venue;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import mitch.eventmanager.API.APIClient;
import mitch.eventmanager.API.Interfaces.EventInterface;
import mitch.eventmanager.API.Response.SimpleResponse;
import mitch.eventmanager.BaseActivity;
import mitch.eventmanager.Misc.Snackbox;
import mitch.eventmanager.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VenueDetail extends BaseActivity {

    @BindView(R.id.venue_detail_going)
    public TextView goingLabel;
    @BindView(R.id.venue_detail_maybe)
    public TextView maybeLabel;
    @BindView(R.id.venue_detail_not_going)
    public TextView notgoingLabel;
    @BindView(R.id.venue_detail_place_address)
    public TextView address;
    @BindView(R.id.venue_detail_place_name)
    public TextView placename;
    @BindView(R.id.venue_detail_nickname)
    public TextView nickname;
    @BindView(R.id.venue_detail_description)
    public TextView description;
    //This one needs BaseActivity for AuthHeaders
    private String venueID;
    private String token;

    private EventInterface eventInterface;

    @SuppressLint("MissingSuperCall")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_venue_detail);
        initForm();

        if (getIntent() == null) {
            finishActivity(999);
        }

        token = getIntent().getStringExtra("token");
        Venue venue = (Venue) getIntent().getSerializableExtra("venue");

        eventInterface = APIClient.getClient(URL).create(EventInterface.class);

        setupData(venue);

    }

    private void setupData(Venue venue) {

        venueID = venue.getVenueID();

        nickname.setText(venue.getVenueNickname());
        placename.setText(venue.getAddress().getPlaceName());
        address.setText(venue.getAddress().getAddress());

        if (venue.getVenueDescription().equals("")) {
            description.setText("No Description For This Venue");
        } else {
            description.setText(venue.getVenueDescription());
        }


    }


    private void setupInvite(String inviteString) {
        try {

            JSONObject inviteObj = new JSONObject(inviteString);
            int response = inviteObj.getInt("userResponse");

            JSONArray generalInvite = inviteObj.getJSONArray("generalInvite");

            for (int x = 0; x < generalInvite.length(); x++) {

            }

            setBackgroundHighlight(response);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupResponseList() {

    }

    private void initForm() {
        goingLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeUserResponse(2);
            }
        });
        maybeLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeUserResponse(1);
            }
        });
        notgoingLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeUserResponse(0);
            }
        });
    }

    private void resetBackground() {
        notgoingLabel.setBackgroundColor(getColor(R.color.white));
        maybeLabel.setBackgroundColor(getColor(R.color.white));
        goingLabel.setBackgroundColor(getColor(R.color.white));
    }

    private void setBackgroundHighlight(int response) {
        resetBackground();
        switch (response) {
            case 0:
                notgoingLabel.setBackgroundColor(getColor(R.color.highlight));
                break;
            case 1:
                maybeLabel.setBackgroundColor(getColor(R.color.highlight));
                break;
            case 2:
                goingLabel.setBackgroundColor(getColor(R.color.highlight));
                break;
        }

    }


    private void changeUserResponse(final int userResponse) {


        final Snackbox snack = new Snackbox(this);

        Map<String, String> params = new HashMap<>();
        params.put("venueID", venueID);
        params.put("response", Integer.toString(userResponse));

        String url = "https://event.thecheapestpint.co.uk/api/invite/venue";

        Call<SimpleResponse> responseCall = eventInterface.changeVenueResponse(getAuthHeader(), venueID, userResponse);

        responseCall.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {

                if (!response.isSuccessful() || !response.body().isSuccess()) {

                    setBackgroundHighlight(userResponse);

                }

            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {

            }
        });


    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
