package mitch.eventmanager.Event.Venue;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import mitch.eventmanager.Event.EventGetPojo.Responses;

public class Venue implements Serializable {
    @SerializedName("venueNickname")
    @Expose
    private String venueNickname;
    @SerializedName("arrival")
    @Expose
    private String arrival;
    @SerializedName("venuePosition")
    @Expose
    private int venuePosition;
    @SerializedName("venueDescription")
    @Expose
    private String venueDescription;
    @SerializedName("venueID")
    @Expose
    private String venueID;
    @SerializedName("address")
    @Expose

    private Address address;
    @SerializedName("responses")
    @Expose

    private Responses responses;

    /**
     * No args constructor for use in serialization
     */
    public Venue() {
        address = new Address();
    }

    /**
     * @param address
     * @param arrival
     * @param venuePosition
     * @param venueNickname
     * @param responses
     * @param venueID
     * @param venueDescription
     */
    public Venue(String venueNickname, String arrival, int venuePosition, String venueDescription, String venueID, Address address, Responses responses) {
        super();
        this.venueNickname = venueNickname;
        this.arrival = arrival;
        this.venuePosition = venuePosition;
        this.venueDescription = venueDescription;
        this.venueID = venueID;
        this.address = address;
        this.responses = responses;
    }

    public String getVenueNickname() {
        return venueNickname;
    }

    public void setVenueNickname(String venueNickname) {
        this.venueNickname = venueNickname;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public int getVenuePosition() {
        return venuePosition;
    }


    public void setVenuePosition(int x) {
        this.venuePosition = x;
    }

    public String getVenueDescription() {
        return venueDescription;
    }

    public void setVenueDescription(String venueDescription) {
        this.venueDescription = venueDescription;
    }

    public String getVenueID() {
        return venueID;
    }

    public void setVenueID(String venueID) {
        this.venueID = venueID;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Responses getResponses() {
        return responses;
    }

    public void setResponses(Responses responses) {
        this.responses = responses;
    }

    public boolean isEmpty() {
        //Longitude and Latitude need to be initialised.
        return this.address.getLatitude() == null || this.address.getLongitude() == null;
    }

}
