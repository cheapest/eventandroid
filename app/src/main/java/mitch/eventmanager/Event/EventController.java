package mitch.eventmanager.Event;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONObject;

import java.util.ArrayList;

import mitch.eventmanager.API.APIClient;
import mitch.eventmanager.API.Interfaces.EventInterface;
import mitch.eventmanager.API.Response.EventResponse.EventResponse;
import mitch.eventmanager.BaseActivity;
import mitch.eventmanager.Event.EventGetPojo.Event;
import mitch.eventmanager.Event.Fragments.EventChatFragment;
import mitch.eventmanager.Event.Fragments.EventMainFragment;
import mitch.eventmanager.Event.Fragments.EventMapFragment;
import mitch.eventmanager.Login;
import mitch.eventmanager.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventController extends BaseActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private String eventID;

    private EventInterface eventInterface;

    @SuppressLint("MissingSuperCall")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_event_controller);

        Intent i;

        if (!isLoggedin) {
            i = new Intent(this, Login.class);
            startActivity(i);
        } else {
            i = getIntent();
            eventID = i.getStringExtra("eventID");

            eventInterface = APIClient.getClient(URL).create(EventInterface.class);

            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

            ViewPager mViewPager = (ViewPager) findViewById(R.id.container);
            mViewPager.setOffscreenPageLimit(3);
            mViewPager.setAdapter(mSectionsPagerAdapter);


            TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(mViewPager);

            getEventInfo();
        }

    }


    private void getEventInfo() {
        Call<EventResponse> getEvent = eventInterface.getEvent(getAuthHeader(), eventID);

        getEvent.enqueue(new Callback<EventResponse>() {
            @Override
            public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {

                EventResponse eventResponse = response.body();

                if (!response.isSuccessful() || !eventResponse.getSuccess()) {
                    snackbox.showMessage("Error", eventResponse.getMessage());
                } else {
                    setupEvent(eventResponse.getEvent());
                }
            }

            @Override
            public void onFailure(Call<EventResponse> call, Throwable t) {
                t.printStackTrace();
                snackbox.showMessage("Error", t.getMessage());
            }
        });

    }

    private void setupEvent(Event response) {
            EventMainFragment infoFrag = (EventMainFragment) mSectionsPagerAdapter.getItem(0);
            infoFrag.sendData(response, token);

            EventMapFragment map = (EventMapFragment) mSectionsPagerAdapter.getItem(1);
            map.setUserLocation(mLatitude, mLongitude);
            updateFragmentLocation();
        map.sendData(response.getVenueInfo());

            //EventChatFragment inv = (EventChatFragment) mSectionsPagerAdapter.getItem(2);
            //inv.sendData(invite);
    }

    @Override
    public void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }


    private void updateFragmentLocation() {
        final Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                EventMapFragment map = (EventMapFragment) mSectionsPagerAdapter.getItem(1);
                map.setUserLocation(mLatitude, mLongitude);
            }
        };
        handler.postDelayed(r, 100);
    }

    private void setupInvite(JSONObject inviteData) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event_controller, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private ArrayList<Fragment> frags;


        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            frags = new ArrayList<>();
            frags.add(EventMainFragment.newInstance());
            frags.add(EventMapFragment.newInstance());
            frags.add(EventChatFragment.newInstance());
        }

        @Override
        public Fragment getItem(int position) {
            return frags.get(position);
        }

        @Override
        public int getCount() {
            return frags.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Event Information";
                case 1:
                    return "Map";
                case 2:
                    return "Chat";
            }
            return null;
        }


    }
}