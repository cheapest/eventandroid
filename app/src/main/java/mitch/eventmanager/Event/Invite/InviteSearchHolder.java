package mitch.eventmanager.Event.Invite;

/**
 * Created by paulmitchell on 26/04/2017.
 */

public class InviteSearchHolder {

    private String username;
    private String email;
    private String socialUserID;
    private String profilePic;
    private String simpleUserID;
    private boolean emptyResult;


    public InviteSearchHolder(boolean empty) {
        this.emptyResult = empty;
    }

    public InviteSearchHolder(String username, String email, String socialUserID, String profilePic) {
        this.username = username;
        this.email = email;
        this.socialUserID = socialUserID;
        this.profilePic = profilePic;
    }

}
