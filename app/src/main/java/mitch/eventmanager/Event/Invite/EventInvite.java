package mitch.eventmanager.Event.Invite;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import mitch.eventmanager.BaseActivity;
import mitch.eventmanager.Login;
import mitch.eventmanager.R;

public class EventInvite extends BaseActivity {

    @SuppressLint("MissingSuperCall")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_event_invite);

        if (!isLoggedin) {
            Intent i = new Intent(this, Login.class);
            startActivity(i);
            finish();
        }

    }

}
