package mitch.eventmanager.Event.Invite;

public class InviteHolder {
    private String name;
    private String image;
    private String response;

    public InviteHolder(String name, String image, String response) {
        this.name = name;
        this.image = image;
        this.response = response;
    }

    public InviteHolder(String name, String response) {
        this.name = name;
        this.response = response;
    }

    public String getUserName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getResponse() {
        return response;
    }
}
