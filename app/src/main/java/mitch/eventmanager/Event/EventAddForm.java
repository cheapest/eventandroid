package mitch.eventmanager.Event;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import mitch.eventmanager.Event.Venue.Venue;
import mitch.eventmanager.Misc.DateTime;
import mitch.eventmanager.Misc.Snackbox;
import mitch.eventmanager.R;

public class EventAddForm extends AppCompatActivity {

    private Venue venue;
    private Button arrivalButton;
    private EditText nameEdit;
    private EditText venueDescription;
    private Snackbox snackbox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_add_form);

        nameEdit = findViewById(R.id.placeName);
        venueDescription = findViewById(R.id.venueDescription);
        arrivalButton = findViewById(R.id.arrivalDateTime);
        arrivalButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                dateTimeOne();
            }
        });
        snackbox = new Snackbox(this);
        addressSetup();
        //Put everything in this venue holder.
        venue = new Venue();
    }

    private Double decimal(Double coordinate) {
        String convert = new BigDecimal(coordinate).toPlainString();
        return Double.parseDouble(convert);
    }

    public void addressSetup() {
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                getRealPlaceInfo(place);
                Log.i("AutoComplete", "Place: " + place.getName());
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("AutoComplete", "An error occurred: " + status);
            }
        });
    }

    private void getRealPlaceInfo(Place place) {
        //This will use another API credit but f it
        venue.getAddress().setPlaceName(place.getName().toString());
        venue.getAddress().setAddress(place.getAddress().toString());
        venue.getAddress().setLatitude(decimal(place.getLatLng().latitude));
        venue.getAddress().setLongitude(decimal(place.getLatLng().longitude));
        Geocoder geo = new Geocoder(this, Locale.getDefault());

        try {
            /*The purpose of this bit is to get the separate geo information for searching.*/
            List<Address> addressList = geo.getFromLocation(venue.getAddress().getLatitude(), venue.getAddress().getLongitude(), 1);
            //venue.setPlaceAddress(addressList.get(0).getAddressLine(0));
            venue.getAddress().setPostcode(addressList.get(0).getPostalCode());
            venue.getAddress().setCity(addressList.get(0).getLocality());
            venue.getAddress().setRegion(addressList.get(0).getAdminArea());
            venue.getAddress().setCountry(addressList.get(0).getCountryName());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void dateTimeOne() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String date = checkAndAppend(dayOfMonth) + "/" + checkAndAppend(monthOfYear + 1) + "/" + year;
                        timeOne(date);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private String checkAndAppend(int check) {
        String appended;
        if (check < 10) {
            appended = "0" + check;
        } else {
            appended = "" + check;
        }

        return appended;
    }

    public void timeOne(final String date) {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR);
        int mMinute = c.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String timeOne = checkAndAppend(selectedHour) + ":" + checkAndAppend(selectedMinute) + ":00";
                if(DateTime.compareDates(date + " " + timeOne)) {
                    snackbox.showMessage("","Please select a later time. Time must be in the future.");
                } else {
                    arrivalButton.setText(String.format("%s %s", date, timeOne));
                    venue.setArrival(date + " " + timeOne);
                }
            }
        }, mHour, mMinute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public void submit(View view) {
        Snackbox snack = new Snackbox(this);
        if (venue.getArrival().equals("")) {
            snack.showMessage("Cannot Continue", "Please enter a start time.");
        } else {
            returnData();
        }
    }


    private void returnData() {
        venue.setVenueNickname(nameEdit.getText().toString());
        venue.setVenueDescription(venueDescription.getText().toString());
        Intent returnIntent = new Intent();
        returnIntent.putExtra("venue", venue);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }


}
