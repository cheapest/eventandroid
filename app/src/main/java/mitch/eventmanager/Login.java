package mitch.eventmanager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import mitch.eventmanager.API.APIClient;
import mitch.eventmanager.API.Interfaces.LoginInterface;
import mitch.eventmanager.API.Response.LoginResponse;
import mitch.eventmanager.Misc.Dialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends BaseActivity {
    @BindView(R.id.loginUsername)
    public EditText emailLabel;
    @BindView(R.id.loginPassword)
    public EditText passwordLabel;
    private CallbackManager callbackManager;
    private Dialog dialog;
    private LoginInterface loginInterface;

    @SuppressLint("MissingSuperCall")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.login);


        loginInterface = APIClient.getClient(super.URL).create(LoginInterface.class);

        //TextView forgot = (TextView) findViewById(R.id.forgotButton);
        /*forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //toForgetPage();
            }
        });
*/
        if (isLoggedin) {
            Intent i = new Intent(this, Home.class);
            startActivity(i);
            finish();


        }
        facebookSetUp();
        // calculateHashKey("mitch.eventmanager");
    }


    private void calculateHashKey(String yourPackageName) {
        try {
            @SuppressLint("PackageManagerGetSignatures") PackageInfo info = getPackageManager().getPackageInfo(
                    yourPackageName,
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String key = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.d("KeyHash:", key);
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }


    private void facebookSetUp() {
        callbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_btn_facebook);
        loginButton.setReadPermissions(Collections.singletonList("public_profile, email, user_friends"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                AccessToken userAccessToken = loginResult.getAccessToken();
                final String socialUserId = userAccessToken.getUserId();
                GraphRequest gr = GraphRequest.newMeRequest(userAccessToken,
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());
                                try {
                                    //String userID = object.getString("id");
                                    String email = object.getString("email");
                                    facebookLogin(email, socialUserId);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, name, email, gender, birthday");
                gr.setParameters(parameters);
                gr.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.v("LoginActivity", "cancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.v("LoginActivity", error.getCause().toString());
            }

        });
    }

    public void facebookLogin(String email, final String userId) {
        Call<LoginResponse> call = loginInterface.socialLogin("Facebook", "true", email, userId);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {


                if (!response.isSuccessful() || (!response.body().isSuccess())) {

                    dialog.showMessage("Could Not Log You In.", response.body().getMessage());

                } else {
                    LoginResponse loginResponse = response.body();

                    Map<String, String> results = new HashMap<>();
                    results.put("token", loginResponse.getToken());
                    results.put("expiryDate", loginResponse.getExpiryDate());
                    results.put("userType", loginResponse.getUserType());
                    results.put("socialLogin", "true");
                    results.put("socialUserId", userId);
                    completeLogin(results);

                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("FAILURE", "Couldn't log in");
                dialog.showMessage("Error Result", t.getMessage());
            }
        });

    }

    public void completeLogin(Map<String, String> res) {
        sm.createLoginSession(res);
        Intent i = new Intent(this, Home.class);
        startActivity(i);
        finish();
    }

    public void normalLogin(View view) {
        if (checkEmpties()) {
            String email = emailLabel.getText().toString();
            String password = passwordLabel.getText().toString();

            Call<LoginResponse> call = loginInterface.login(email, password);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {


                    if (!response.isSuccessful() || (!response.body().isSuccess())) {

                        dialog.showMessage("Could Not Log You In.", response.body().getMessage());

                    } else {
                        LoginResponse loginResponse = response.body();

                        Map<String, String> results = new HashMap<>();
                        results.put("token", loginResponse.getToken());
                        results.put("expiryDate", loginResponse.getExpiryDate());
                        results.put("userType", loginResponse.getUserType());
                        results.put("socialUserId", "");
                        completeLogin(results);

                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e("FAILURE", "Couldn't log in");
                    dialog.showMessage("Error Result", t.getMessage());
                }
            });
        }
    }

    private boolean checkEmpties() {
        if (emailLabel.getText().toString().equals("")) {
            emailLabel.setError("Please Enter An Email.");
        } else if (passwordLabel.getText().toString().equals("")) {
            passwordLabel.setError("Please Enter A Password.");
        } else {
            return true;
        }

        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    public void emailRegistration(View view) {
        // toRegister();
    }


}
