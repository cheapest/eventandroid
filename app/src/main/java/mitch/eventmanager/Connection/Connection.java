package mitch.eventmanager.Connection;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;


public class Connection {
    private Context c;
    private boolean isConnected;
    private boolean isWifi;
    private boolean isMobile;


    public Connection(Context con){
        c = con;
    }

    public void isConnectToNetwork() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            postLollipop();
        } else {
            preLollipop();
        }
    }


    @SuppressWarnings("deprecation")
    private void preLollipop(){
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI")) {
                if (ni.isConnected()) {
                    isConnected = true;
                    isWifi = true;
                    isMobile = false;
                }
            }
            if (ni.getTypeName().equalsIgnoreCase("MOBILE")) {
                if (ni.isConnected()) {
                    isConnected = true;
                    isWifi = false;
                    isMobile = true;
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void postLollipop(){
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                isConnected = true;
                isMobile = false;
                isWifi = true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to mobile
                isConnected = true;
                isMobile = true;
                isWifi = false;
            }
        } else {
            // not connected to the internet
            isConnected = false;
            isWifi = false;
            isConnected = false;
        }
    }


    public boolean isMobile() {
        return isMobile;
    }

    public boolean isWifi() {

        return isWifi;
    }

    public boolean isConnected() {

        return isConnected;
    }


}
