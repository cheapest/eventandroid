package mitch.eventmanager.SQL;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Mitchell on 28/08/2017.
 */

@Entity
public class LocationEntity {


    @PrimaryKey(autoGenerate = true)
    private int locationID;

    @ColumnInfo(name = "event_id")
    private String eventID;

    @ColumnInfo(name = "user_id")
    private String userID;

    @ColumnInfo(name = "longitude")
    private String userLongitude;

    @ColumnInfo(name = "latitude")
    private String userLatitude;

    @ColumnInfo(name = "last_updated")
    private String lastUpdated;

}
