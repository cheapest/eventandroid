package mitch.eventmanager.Form.Spinner;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import mitch.eventmanager.R;

/**
 * Created by paulmitchell on 11/03/2017.
 */

public class BigSpinnerWithIcon extends BaseAdapter {

    private Context con;

    private ArrayList<Integer> icons;
    private ArrayList<String> strings;

    public BigSpinnerWithIcon(Context c) {
        con = c;
    }

    @Override
    public int getCount() {
        return icons.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = View.inflate(con, R.layout.big_spinner, parent);
        TextView text = (TextView) convertView.findViewById(R.id.big_spinner_label);
        ImageView icon = (ImageView) convertView.findViewById(R.id.big_spinner_image);

        text.setText(strings.get(position));
        icon.setImageResource(icons.get(position));

        return null;
    }
}
