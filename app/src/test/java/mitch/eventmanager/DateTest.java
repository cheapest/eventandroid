package mitch.eventmanager;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

import mitch.eventmanager.Event.Venue.Venue;
import mitch.eventmanager.Event.Venue.VenueComparator;

import static org.junit.Assert.assertTrue;

/**
 * Created by Mitchell on 25/02/2017.
 */

public class DateTest {

    @Test
    public void date_sort() throws Exception {

        ArrayList<Venue> sortedVenues = new ArrayList<>();
        sortedVenues.add(0,new Venue("","","","","","","","",0.00,0.00,"2017-02-23 23:00:00",0));
        sortedVenues.add(1,new Venue("","","","","","","","",0.00,0.00,"2017-02-23 03:00:00",0));
        sortedVenues.add(2,new Venue("","","","","","","","",0.00,0.00,"2017-02-23 11:00:00",0));
        sortedVenues.add(3,new Venue("","","","","","","","",0.00,0.00,"2017-02-23 16:00:00",0));
        sortedVenues.add(4,new Venue("","","","","","","","",0.00,0.00,"2017-02-25 01:00:00",0));

        ArrayList<Venue> venues = new ArrayList<>();
        venues.add(0,new Venue("","","","","","","","",0.00,0.00,"2017-02-23 16:00:00",0));
        venues.add(1,new Venue("","","","","","","","",0.00,0.00,"2017-02-23 11:00:00",0));
        venues.add(2,new Venue("","","","","","","","",0.00,0.00,"2017-02-23 03:00:00",0));
        venues.add(3,new Venue("","","","","","","","",0.00,0.00,"2017-02-23 23:00:00",0));
        venues.add(4,new Venue("","","","","","","","",0.00,0.00,"2017-02-25 01:00:00",0));

        Collections.sort(venues,new VenueComparator());

        assertTrue(sortedVenues.equals(venues));
    }

}
